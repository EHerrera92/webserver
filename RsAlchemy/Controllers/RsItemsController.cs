﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RsAlchemy.Models;
using Serilog;

namespace RsAlchemy
{
    public class RsItemsController : Controller
    {
        public static int itemLimit = 200;
        private static readonly ILogger logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.Async(a => a.File(path: "/logs/log.txt", rollOnFileSizeLimit: true, fileSizeLimitBytes: 1024000L, outputTemplate: "{Timestamp: yyyy-MM-dd HH:mm:ss} [{Level:u3}] {Message}{NewLine}")).CreateLogger();
        private readonly IConfiguration cfg;
        private static SQLiteConnection conn = null;
        public RsItemsController(IConfiguration cfg)
        {
            this.cfg = cfg;
            itemLimit = cfg.GetValue<int>("ItemLimit");
            if(conn == null)
            {
                string dbPath = cfg.GetValue<string>("DbPath");
                if (!System.IO.File.Exists(dbPath))
                {
                    System.IO.Directory.CreateDirectory(dbPath.Substring(0, dbPath.LastIndexOf('/')));
                    System.IO.File.Create(dbPath);
                }
                conn = new SQLiteConnection($"Data Source={cfg.GetValue<string>("DbPath")};Version=3;");
                conn.Open();
            }
        }
        [Route("api/fetchitems")]
        public async Task<JsonResult> fetchItems(string f2pOnly)
        {
            logger.Information($"fetchItems with f2pOnly {f2pOnly} from ip {this.Request.Host.Host}");
            SQLiteCommand cmd = null;
            DbDataReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                string sql = "WITH TMP AS (SELECT ICON, NAME, PRICE, HI_ALCH, BUY_LIMIT, RS_ID FROM ITEM";
                if ("Yes" == f2pOnly)
                    sql += " WHERE MEMBERS = 0";
                sql += $"  ORDER BY (HI_ALCH - PRICE) DESC LIMIT {itemLimit}) SELECT * FROM TMP UNION SELECT ICON, NAME, PRICE, HI_ALCH, BUY_LIMIT, RS_ID FROM ITEM WHERE NAME = 'Nature rune'";
                cmd.CommandText = sql;
                reader = await cmd.ExecuteReaderAsync();
                List<RsItem> results = new List<RsItem>();
                while (await reader.ReadAsync())
                {

                    results.Add(new RsItem()
                    {
                        icon = reader.GetString(0),
                        name = reader.GetString(1),
                        price = reader.GetInt32(2),
                        hiAlch = reader.GetInt32(3),
                        buyLimit = reader.GetInt32(4),
                        rsId = reader.GetInt32(5)
                    });
                }
                return Json(results);
            }
            catch(Exception e)
            {
                logger.Debug($"Exception while retrieving item data. f2pOnly was {f2pOnly} - {e.Message} {e.StackTrace}");
                return Json(new object[] { });
            }
            finally
            {
                if(reader != null)
                    reader.Close();
                if(cmd != null)
                    cmd.Dispose();
                if(conn !=  null)
                    conn.Dispose();
            }
        }
    }
}
