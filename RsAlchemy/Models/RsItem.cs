﻿using System;
namespace RsAlchemy.Models
{
    public class RsItem
    {
        public string icon { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public int hiAlch { get; set; }
        public int buyLimit { get; set; }
        public int rsId { get; set; }
    }
}
