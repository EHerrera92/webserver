﻿$(document).ready(function () {
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    $('.glyphicon-question-sign').click(function (evt) {
        switch ($(evt.target).attr('modal-info')) {
            case 'HighAlchValue':
                $('#modalTitle').text("High Alch Value");
                $('#modalBody').text("This is how much money you get from using the High-Level Alchemy spell on the item. Requires level 55 magic");
                break;
            case 'BuyLimit':
                $('#modalTitle').text("Buy Limit");
                $('#modalBody').text("The Grand Exchange limits how many items you can buy every 4 hours. This limit is tracked separately for each item. For example if Item A has a limit of 100 and Item B has a limit of 50, you could buy 100 Item A and 50 Item B in a single 4 hour period.");
                break;
            case 'Profit':
                $('#modalTitle').text("Profit");
                $('#modalBody').text("This is how much profit you make from each high-alchemy spell you perform on the item. It's calculated as [High Alch Value] - [GE Cost] - [Nature Rune Cost]. This assumes you're using a Fire Staff.");
                break;
            case 'PercentProfit':
                $('#modalTitle').text("Percent Profit");
                $('#modalBody').text("This shows how much profit you make per gp. If you ignore buy limits then this is the best metric to use.");
                break;
            case 'MaxProfit':
                $('#modalTitle').text("Max Profit");
                $('#modalBody').text("This is the maximum amount of profit you can make every 4 hours by high-alching this item. You might make less than this depending on how much GP you have available.");
                break;
            case 'YourProfit':
                $('#modalTitle').text("Your Profit");
                $('#modalBody').text("This takes the number you entered in the 'Your GP' box and calculates the maximum profit you could make every 4 hours by high-alching the item. If you have a low amount of GP then your best options will be different from someone that has a large amount of GP to invest.");
                break;
            default:
                alert('lul');
        }
        $('#myModal').modal();
        return false;
    });
    function updateTable(init) {
        f2p = getCookie("f2pOnly");
        $.ajax({
            type: "POST",
            url: "api/fetchitems",
            data: { f2pOnly: f2p },
            success: function (result) {
                var natureRune = result.filter(item => item.name == 'Nature rune')[0];
                if (!natureRune || result.length == 0) {
                    $('#modalTitle').text("Error");
                    $('#modalBody').text("There was an error retrieving GE information. Please try again later.");
                    $('#myModal').modal();
                    return;
                }
                $('#imgNatureRune').attr('src', natureRune.icon);
                document.getElementById('txtNatureRune').innerText = String(natureRune.price);
                mainTable.clear();
                var currentGp = $('#currentGp').val();
                result.forEach(item => {
                    if (item.name == "Nature rune")
                        return;
                    mainTable.row.add([
                        '<a href=http://services.runescape.com/m=itemdb_oldschool/viewitem?obj=' + item.rsId + ' target="_blank"><img src="' + item.icon + '"/></a>',
                        '<span style="display:none;">' + item.name + '</span><a href=http://services.runescape.com/m=itemdb_oldschool/viewitem?obj=' + item.rsId + ' target="_blank">' + item.name + '</a>',
                        item.price,
                        item.hiAlch,
                        item.buyLimit,
                        item.hiAlch - item.price - natureRune.price,
                        (100.0 * item.hiAlch / (item.price + natureRune.price)) - 100.0,
                        (item.hiAlch - item.price - natureRune.price) * item.buyLimit
                    ])
                });
                if(init)
                    mainTable.order([7, 'desc'])
                mainTable.draw();
            }
        });
    }
    //Initialize the main table
    mainTable = $('#MainTable').DataTable({
        columns: [
            { type: "html", orderable: false },
            { type: "string" },
            { "type": "num-fmt", render: $.fn.dataTable.render.number(',', '.', 0) },
            { "type": "num-fmt", render: $.fn.dataTable.render.number(',', '.', 0) },
            { "type": "num-fmt", render: $.fn.dataTable.render.number(',', '.', 0) },
            { "type": "num-fmt", render: $.fn.dataTable.render.number(',', '.', 0) },
            { "type": "num-fmt", render: $.fn.dataTable.render.number(',', '.', 2, null, '%') },
            { "type": "num-fmt", render: $.fn.dataTable.render.number(',', '.', 0) }
        ],
        lengthChange: false,
        pageLength: 25,
        searching: false
    });
    if (getCookie("f2pOnly") == "Yes")
        document.getElementById("f2pOnlyYes").checked = true;
    updateTable(true);
    $('input[type="radio"]').click(evt => {
        document.cookie = "f2pOnly=" + evt.target.value + "; expires = Tue, 19 Jan 2037 03: 14: 07 UTC";
        updateTable(evt.target.value);
    });
});